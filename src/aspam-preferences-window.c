/* aspam-preferences-window.h
 *
 * Copyright 2021-2022 Chris Talbot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "aspam-preferences-window.h"
#include "aspam-settings.h"
#include "aspam-client.h"

struct _ASpamPreferencesWindow
{
  AdwPreferencesDialog parent_instance;

  //Appearance Settings
  AdwActionRow *enable_dark_theme_row;
  GtkSwitch *enable_dark_theme_switch;
};

G_DEFINE_TYPE (ASpamPreferencesWindow, aspam_preferences_window, ADW_TYPE_PREFERENCES_DIALOG)

ASpamPreferencesWindow *
aspam_preferences_window_new (void)
{
  return g_object_new (ADW_TYPE_DEMO_PREFERENCES_WINDOW, NULL);
}

static gboolean
enable_theme_switch_flipped_cb (GtkSwitch   *widget,
                                gboolean     prefer_dark_theme,
                                ASpamPreferencesWindow *self)
{
  ASpamSettings *settings = aspam_settings_get_default ();

  aspam_settings_set_dark_theme (settings, prefer_dark_theme);
  return FALSE;
}

static void
aspam_window_populate (ASpamPreferencesWindow *self)
{
  ASpamSettings *settings = aspam_settings_get_default ();
  g_assert (ADW_IS_PREFERENCES_DIALOG (self));

  gtk_switch_set_active (GTK_SWITCH (self->enable_dark_theme_switch),
                         aspam_settings_get_dark_theme (settings));
}

static void
aspam_preferences_window_class_init (ASpamPreferencesWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/kop316/antispam/ui/aspam-preferences-window.ui");

  //Appearance Settings
  gtk_widget_class_bind_template_child (widget_class, ASpamPreferencesWindow, enable_dark_theme_switch);
  gtk_widget_class_bind_template_child (widget_class, ASpamPreferencesWindow, enable_dark_theme_row);

  gtk_widget_class_bind_template_callback (widget_class, enable_theme_switch_flipped_cb);
}

static void
aspam_preferences_window_init (ASpamPreferencesWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  aspam_window_populate (self);

}
