phosh-antispam (3.5)

  [ Chris Talbot ]
  * Update to use libadwaita 1.5 APIs

phosh-antispam (3.4)

  [ Chris Talbot ]
  * Make friendlier to translating
  * Update to use libadwaita 1.4 APIs

phosh-antispam (3.3.1)

  [ Chris Talbot ]
  * Fix bug for blank numbers

  [ Peter M ]
  * Fix Meta Info

phosh-antispam (3.3.0)

  [ Chris Talbot ]
  * Add Debug Info

  [ Brage Fuglseth ]
  * Redesign Icon

phosh-antispam (3.2.0)

  [ Chris Talbot and Guido Gunther ]
  * Redesign UI

phosh-antispam (3.1.0)

  [ Chris Talbot ]
  * Various bug fixes
  * Use adw_show_about_window()

phosh-antispam (3.0.0)

  [ Chris Talbot ]
  * Various bug fixes

phosh-antispam (3.0~beta1)

  [ Chris Talbot ]
  * Various bug fixes
  * Allow for Dark Theme

phosh-antispam (3.0~beta)

  [ Chris Talbot ]
  * Rewrite in GTK-4 and libadwaita

phosh-antispam (2.1.1)

  [ Chris Talbot ]
  * Fix some memory leaks

phosh-antispam (2.1)

  [ Chris Talbot ]
  * Various clean ups
  * Allow Antispam to use Blacklist

phosh-antispam (2.0)

  [ Chris Talbot ]
  * Clean up GUI
  * Ensure hang up is used as backup
  * Always hang up on "Spam" contact

phosh-antispam (2.0~beta)

  [ Chris Talbot ]
  * Reintegrate everything into the GUI

phosh-antispam (2.0~alpha)

  [ Chris Talbot ]
  * Rebase for a GUI

phosh-antispam (1.1)

  [ Chris Talbot ]
  * Bug Fixes

phosh-antispam (1.0)

  [ Chris Talbot ]
  * Initial Release
